package com.epam.regexps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberFinder {

    private String writesTextToString(String path) {
        String textMixed = "";
        try {
            textMixed = new TextReader().readsFromFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textMixed;
    }

    List<String> writesPhoneNumberToList(String path, String regexp) {
        String textMixed = writesTextToString(path);
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(textMixed);
        List<String> phoneNumbersList = new ArrayList<>();
        String phoneNumber;
        while (matcher.find()) {
            phoneNumber = matcher.group();
            phoneNumber = phoneNumber.replaceAll("[()]", "").replace("+", "").replace(" ", "");
            phoneNumbersList.add(phoneNumber);
        }
        return phoneNumbersList;
    }
}
