package com.epam.regexps;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class PhoneNumberWriter {

    void writesToFile(List<String> listPhoneNumber, String outputPath)
            throws IOException {
        BufferedWriter bufferedWriter =
                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath), "UTF-8"));
        for (String number : listPhoneNumber) {
            bufferedWriter.write(number.concat("\r\n"));
            bufferedWriter.flush();
        }
    }
}
