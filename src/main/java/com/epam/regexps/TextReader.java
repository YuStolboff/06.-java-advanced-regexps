package com.epam.regexps;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class TextReader {

    String readsFromFile(String path) throws IOException, NullPointerException {
        String textMixed;
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        StringBuilder stringBuilder = new StringBuilder();
        while ((textMixed = bufferedReader.readLine()) != null) {
            stringBuilder.append(textMixed);
        }
        return String.valueOf(stringBuilder);
    }
}
