package com.epam.regexps;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        PhoneNumberWriter phoneNumberWriter = new PhoneNumberWriter();
        String regexp = "\\+[1-9][0-9]{0,2}\\(\\d{3}\\) \\d{3} \\d{2} \\d{2}";
        try {phoneNumberWriter.writesToFile(new PhoneNumberFinder()
                        .writesPhoneNumberToList("textMixedWithPhoneNumbers.txt", regexp), "phoneNumbers.txt");
        } catch (IOException e) {
            System.out.println("Файл не найден или путь к файлу указан неверно");
        }
    }
}
