#Practice

###Regexps


You have file with some text mixed with phone numbers in the following format:  
+Y(XXX) XXX XX XX Sample line of text: "Notification has been sent to +4(351) 455 22 44 successfully.  
+1(431) 542 56 12 is unreachable" Using regexp and matchers process such file and create  
a new one with phone numbers in format of YXXXXXXXXXX.  